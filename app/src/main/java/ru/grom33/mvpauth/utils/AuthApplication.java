package ru.grom33.mvpauth.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by grom on 01.11.2016.
 */

public class AuthApplication extends Application{
    public static SharedPreferences sSharedPreferences;
    private static Context mContext;


    @Override
    public void onCreate() {
        super.onCreate();
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public static  SharedPreferences getSharedPreferences(){
        return sSharedPreferences;
    }

    public static Context getmContext(){
        return mContext;
    }
}
