package ru.grom33.mvpauth.utils.text;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

/**
 * Created by grom on 31.10.2016.
 */

public class AuthWatcher implements TextWatcher {
    private EditText mEditText;
    private EditText mShowMessage;
    private int mCheckMode; // 0 - check email field; 1 - check pass field;

    public AuthWatcher(EditText mEdText, EditText mMess, int mode) {
        super();
        mEditText = mEdText;
        mShowMessage = mMess;
        mCheckMode = mode;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        switch (mCheckMode) {
            case 0:      // Check e-mail
                if (!mEditText.getText().toString().matches("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b")) {
                    mShowMessage.setVisibility(View.VISIBLE);
                    mShowMessage.setText("Не верный e-mail");
                } else {
                    mShowMessage.setVisibility(View.GONE);
                }
                break;
            case 1:      // Check password
                if (!mEditText.getText().toString().matches("^\\S{8,}$")) {

                    mShowMessage.setVisibility(View.VISIBLE);
                    mShowMessage.setText("Пароль менее 8 символов");
                } else {
                    mShowMessage.setVisibility(View.GONE);
                }
                break;
        }
    }
}
