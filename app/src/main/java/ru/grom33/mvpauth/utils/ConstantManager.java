package ru.grom33.mvpauth.utils;

/**
 * Created by grom on 01.11.2016.
 */

public interface ConstantManager {

    //region ==================== auth ====================
    String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";
    String USER_ID_KEY = "USER_ID_KEY";
    String USER_MAIL_KEY="USER_MAIL_KEY";
    String USER_PASS_KEY="USER_PASS_KEY";
   //endregion


}
