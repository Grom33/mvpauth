package ru.grom33.mvpauth.data;

import java.util.ArrayList;
import java.util.List;

import ru.grom33.mvpauth.data.storage.dto.ProductDto;

/**
 * Created by grom on 29.10.2016.
 */
public class DataManager {
    private static DataManager ourInstance = new DataManager();
    private List<ProductDto> mMockProductList;

    public static DataManager getInstance() {
        return ourInstance;
    }

    private DataManager() {
        generateMockData();

    }

    public ProductDto getProductById(int productID) {
        // TODO: 29.10.2016  this temp sample mock data fix me (may be load from db)
        return mMockProductList.get(productID-1);
    }

    public void updateProduct(ProductDto product) {
        // TODO: 29.10.2016  update product count or status (somethig in product) save in Db
    }
    public List<ProductDto> getProductList(){
        // TODO: 29.10.2016 load product list from anywhere
        return mMockProductList;
    }


    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "test 1", "imageUrl", "desription1 desription1 desription1 desription1 ", 100,1));
        mMockProductList.add(new ProductDto(2, "test 2", "imageUrl", "desription1 desription1 desription1 desription1 ", 200,1));
        mMockProductList.add(new ProductDto(3, "test 3", "imageUrl", "desription1 desription1 desription1 desription1 ", 300,1));
        mMockProductList.add(new ProductDto(4, "test 4", "imageUrl", "desription1 desription1 desription1 desription1 ", 400,1));
        mMockProductList.add(new ProductDto(5, "test 5", "imageUrl", "desription1 desription1 desription1 desription1 ", 500,1));
        mMockProductList.add(new ProductDto(6, "test 6", "imageUrl", "desription1 desription1 desription1 desription1 ", 600,1));
        mMockProductList.add(new ProductDto(7, "test 7", "imageUrl", "desription1 desription1 desription1 desription1 ", 700,1));
        mMockProductList.add(new ProductDto(8, "test 8", "imageUrl", "desription1 desription1 desription1 desription1 ", 800,1));
        mMockProductList.add(new ProductDto(9, "test 9", "imageUrl", "desription1 desription1 desription1 desription1 ", 900,1));
        mMockProductList.add(new ProductDto(10, "test 10", "imageUrl", "desription1 desription1 desription1 desription1 ", 1000,1));

    }

    public boolean isAuthUser() {
        // TODO: 30.10.2016  checked user auth token in shared preference
        return false;
    }
}
