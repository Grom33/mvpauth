package ru.grom33.mvpauth.data.managers;

import android.content.SharedPreferences;

import ru.grom33.mvpauth.utils.AuthApplication;
import ru.grom33.mvpauth.utils.ConstantManager;

/**
 * Created by grom on 01.11.2016.
 */

public class PreferencesManager {
    private SharedPreferences mSharedPreferences;

    public PreferencesManager() {
        this.mSharedPreferences = AuthApplication.getSharedPreferences();
    }

//region ==================== Auth data ====================

    public void saveAuthToken(String authToken) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.AUTH_TOKEN_KEY, authToken);
        editor.apply();
    }
    public String getAuthToken(){
        return mSharedPreferences.getString(ConstantManager.AUTH_TOKEN_KEY,"null");
    }
    public void saveUserPair(String mail, String pass) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.USER_MAIL_KEY, mail);
        editor.putString(ConstantManager.USER_PASS_KEY, pass);
        editor.apply();
    }
    public String getUserMail(){
        return mSharedPreferences.getString(ConstantManager.USER_MAIL_KEY,"null");
    }
    public String getUserPass(){
        return mSharedPreferences.getString(ConstantManager.USER_PASS_KEY,"null");
    }
    //endregion
}
