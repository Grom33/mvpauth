package ru.grom33.mvpauth.ui.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.grom33.mvpauth.R;
import ru.grom33.mvpauth.data.storage.dto.ProductDto;
import ru.grom33.mvpauth.mvp.presenters.ProductPresenter;
import ru.grom33.mvpauth.mvp.presenters.ProductPresenterFactory;
import ru.grom33.mvpauth.mvp.views.IProductView;
import ru.grom33.mvpauth.ui.activities.RootActivity;

/**
 * Created by grom on 30.10.2016.
 */

public class ProductFragment extends Fragment implements IProductView, View.OnClickListener {
    private static final String TAG = "ProductFragment";
    @BindView(R.id.product_name_txt)
    TextView productNameTxt;
    @BindView(R.id.product_description_txt)
    TextView productDescriptionTxt;
    @BindView(R.id.product_image)
    ImageView productImage;
    @BindView(R.id.product_count_txt)
    TextView productCountTxt;
    @BindView(R.id.product_price_txt)
    TextView productPriceTxt;
    @BindView(R.id.plus_btn)
    ImageButton plusBtn;
    @BindView(R.id.minus_btn)
    ImageButton minusBtn;

    // TODO: 30.10.2016 it is temp solution - fix it (picasso)
    @BindDrawable(R.drawable.radio)
    Drawable productDraw;

    private ProductPresenter mPresenter;

    public ProductFragment() {

    }

    public static ProductFragment newInstance(ProductDto product) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("PRODUCT", product);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            ProductDto product = bundle.getParcelable("PRODUCT");
            // TODO: 30.10.2016  init presenter
            mPresenter = ProductPresenterFactory.getInstance(product);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        readBundle(getArguments());
        mPresenter.takeView(this);
        mPresenter.initView();
        plusBtn.setOnClickListener(this);
        minusBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    //region ====================  IProductView ====================
    @Override
    public void showProductView(ProductDto product) {
        productNameTxt.setText(product.getProductName());
        productDescriptionTxt.setText(product.getDescription());
        productCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount()>0){
            productPriceTxt.setText(String.valueOf(product.getCount()*product.getPrice())+".-");
        }else{
            productPriceTxt.setText(String.valueOf(product.getPrice()+".-"));
        }

        // TODO: 30.10.2016  Picasso load from url
        productImage.setImageDrawable(productDraw);

    }

    @Override
    public void updateProductCountView(ProductDto product) {
        productCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount()>0){
            productPriceTxt.setText(String.valueOf(product.getCount()*product.getPrice())+".-");

        }
    }

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }

//endregion

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
            case R.id.plus_btn:
                mPresenter.clickOnPlus();
                break;
         }
    }
}

