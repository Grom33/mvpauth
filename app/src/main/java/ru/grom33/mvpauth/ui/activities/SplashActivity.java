package ru.grom33.mvpauth.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.grom33.mvpauth.BuildConfig;
import ru.grom33.mvpauth.R;
import ru.grom33.mvpauth.mvp.presenters.AuthPresenter;
import ru.grom33.mvpauth.mvp.presenters.IAuthPresenter;
import ru.grom33.mvpauth.mvp.views.IAuthView;
import ru.grom33.mvpauth.ui.custom_views.AuthPanel;

public class SplashActivity extends AppCompatActivity implements IAuthView, View.OnClickListener {
    AuthPresenter mPresenter = AuthPresenter.getInstance();
    protected ProgressDialog mProgressDialog;

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.auth_wrapper)
    AuthPanel mAuthPanel;

    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;

    @BindView(R.id.login_btn)
    Button mLoginBtn;

    //region ==================== Life cycle====================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        mPresenter.takeView(this);
        mPresenter.initView();


        mLoginBtn.setOnClickListener(this);
        mShowCatalogBtn.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    //endregion

    //region ==================== IAuthView ====================
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage("Sorry something wrong");
            // TODO: 23.10.2016 send error stacktrace to crashlytics
        }
    }

    @Override
    public void showLoad() {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_splash);
        } else {
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_splash);
        }
    }

    @Override
    public void hideLoad() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.hide();
            }
        }
    }

    @Override
    public IAuthPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.GONE);
    }

/*
    @Override
    public void testShowLoginCard() {
        mAuthCard.setVisibility(View.VISIBLE);
    }
*/

    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }

    @Override
    public void showCatalogScreen() {
        Intent intent = new Intent(this, RootActivity.class);
        startActivity(intent);
    }
//endregion


    @Override
    public void onBackPressed() {
        if (!mAuthPanel.isIdle()){
            mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);
        }else{

            super.onBackPressed();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                runWithDelay();
                break;
            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();
                break;

        }
    }
    private void runWithDelay() {
        final Handler handler = new Handler();
        showLoad();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideLoad();

            }
        }, 3000);

    }
}
