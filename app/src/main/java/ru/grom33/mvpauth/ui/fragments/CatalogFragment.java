package ru.grom33.mvpauth.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.grom33.mvpauth.R;
import ru.grom33.mvpauth.data.storage.dto.ProductDto;
import ru.grom33.mvpauth.mvp.presenters.CatalogPresenter;
import ru.grom33.mvpauth.mvp.views.ICatalogView;
import ru.grom33.mvpauth.ui.activities.RootActivity;
import ru.grom33.mvpauth.ui.fragments.adapter.CatalogAdapter;

/**
 * Created by grom on 30.10.2016.
 */

public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener {
    private static final String TAG = "CatalogFragment";
    private CatalogPresenter mPresenter = CatalogPresenter.getInstance();
    @BindView(R.id.add_to_card_btn)
    Button addToCardBtn;
    @BindView(R.id.product_pager)
    ViewPager productPager;
    @Nullable
    @BindView(R.id.actionbar_cart_counter_textview)
    TextView mCartCounter;

    public CatalogFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container,false);
        ButterKnife.bind(this,view);
        mPresenter.takeView(this);
        mPresenter.initView();
        addToCardBtn.setOnClickListener(this);
        setHasOptionsMenu(true);
        return view;
    }


    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }



    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_to_card_btn){
           mPresenter.clickOnBayButton(productPager.getCurrentItem());
        }
    }

    //region ==================== ICatalogView ====================
    @Override
    public void showAddCartMessage(ProductDto productDto) {
        showMessage("Good" + productDto.getProductName() + " sucsesfully added to card ");

    }

    @Override
    public void showCatalogView(List<ProductDto> productsList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for (ProductDto product : productsList){
            adapter.addItem(product);
        }
        productPager.setAdapter(adapter);
    }

    @Override
    public void showAuthScreen() {
        // TODO: 30.10.2016  show auth screen if user not auth

    }

    @Override
    public void updateProductCounter(int count) {

// TODO: 30.10.2016  update count product on cart icon
        mCartCounter.setText(Integer.toString(count));
    }
    //endregion

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cart, menu);
        MenuItem item = menu.findItem(R.id.cart_menu);
        MenuItemCompat.setActionView(item, R.layout.menu_cart);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(item);
        TextView tv = (TextView) notifCount.findViewById(R.id.actionbar_cart_counter_textview);
        mCartCounter = tv;
    }

    //region ==================== IView ====================
    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);

    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);

    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();

    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();

    }

//endregion


    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

}
