package ru.grom33.mvpauth.mvp.views;

import java.util.List;

import ru.grom33.mvpauth.data.storage.dto.ProductDto;

/**
 * Created by grom on 30.10.2016.
 */

public interface ICatalogView extends IView {
    void showAddCartMessage(ProductDto productDto);
    void showCatalogView(List<ProductDto> productsList);
    void showAuthScreen();

    void updateProductCounter();

}
