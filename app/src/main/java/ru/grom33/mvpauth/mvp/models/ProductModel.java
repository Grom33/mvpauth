package ru.grom33.mvpauth.mvp.models;

import ru.grom33.mvpauth.data.DataManager;
import ru.grom33.mvpauth.data.storage.dto.ProductDto;

/**
 * Created by grom on 29.10.2016.
 */

public class ProductModel {
    DataManager mDataManager = DataManager.getInstance();
    public ProductDto getProductById(int productID){
        // TODO: 29.10.2016  get product from datamanager
        return mDataManager.getProductById(productID);
    }

    public void updateProduct (ProductDto product){
        mDataManager.updateProduct(product);
    }
}
