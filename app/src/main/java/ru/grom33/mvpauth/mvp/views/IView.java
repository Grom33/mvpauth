package ru.grom33.mvpauth.mvp.views;

/**
 * Created by grom on 29.10.2016.
 */

public interface IView {
    void showMessage(String message);

    void showError(Throwable e);

    void showLoad();

    void hideLoad();
}
