package ru.grom33.mvpauth.mvp.views;

import ru.grom33.mvpauth.data.storage.dto.ProductDto;

/**
 * Created by grom on 29.10.2016.
 */

public interface IProductView extends IView{
    void showProductView(ProductDto product);
    void updateProductCountView(ProductDto product);
}
