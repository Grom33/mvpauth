package ru.grom33.mvpauth.mvp.models;

import java.util.List;

import ru.grom33.mvpauth.data.DataManager;
import ru.grom33.mvpauth.data.storage.dto.ProductDto;

/**
 * Created by grom on 30.10.2016.
 */

public class CatalogModel {
    DataManager mDataManager = DataManager.getInstance();

    public CatalogModel() {
    }

    public List<ProductDto> getProductList(){
        return mDataManager.getProductList();
    }

    public boolean isUserAuth(){
        return mDataManager.isAuthUser();

    }

}
