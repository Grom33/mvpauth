package ru.grom33.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import ru.grom33.mvpauth.mvp.views.IAuthView;

/**
 * Created by grom on 23.10.2016.
 */

public interface IAuthPresenter {
    void takeView(IAuthView authView);
    void dropView();
    void initView();

    @Nullable
    IAuthView getView();

    void clickOnLogin();
    void clickOnFb();
    void clickOnVK();
    void clickOnTwitter();
    void clickOnShowCatalog();

    boolean  checkUserAuth();

}
