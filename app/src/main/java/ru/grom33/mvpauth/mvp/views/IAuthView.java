package ru.grom33.mvpauth.mvp.views;

import android.support.annotation.Nullable;

import ru.grom33.mvpauth.mvp.presenters.IAuthPresenter;
import ru.grom33.mvpauth.ui.custom_views.AuthPanel;

/**
 * Created by grom on 23.10.2016.
 */

public interface IAuthView extends IView{


    IAuthPresenter getPresenter();

    void showLoginBtn();

    void hideLoginBtn();


    //void testShowLoginCard();
    @Nullable
    AuthPanel getAuthPanel();

    void showCatalogScreen();
}
