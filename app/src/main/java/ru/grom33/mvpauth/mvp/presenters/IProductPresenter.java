package ru.grom33.mvpauth.mvp.presenters;

/**
 * Created by grom on 29.10.2016.
 */

public interface IProductPresenter {

    public void clickOnPlus();
    public void clickOnMinus();

}
