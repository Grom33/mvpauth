package ru.grom33.mvpauth.mvp.presenters;

import ru.grom33.mvpauth.mvp.models.AuthModel;
import ru.grom33.mvpauth.mvp.views.IAuthView;
import ru.grom33.mvpauth.ui.custom_views.AuthPanel;

/**
 * Created by grom on 23.10.2016.
 */

public class AuthPresenter extends AbstractPresenter<IAuthView> implements IAuthPresenter {
    private static AuthPresenter ourInstance = new AuthPresenter();
    private AuthModel mAuthModel;
    private IAuthView mIAuthView;

    public AuthPresenter() {
        mAuthModel = new AuthModel();
    }

    public static AuthPresenter getInstance() {
        return ourInstance;
    }


    @Override
    public void initView() {
        if (getView() != null) {
            if (checkUserAuth()) {
                getView().hideLoginBtn();
            } else {
                getView().showLoginBtn();
            }
        }
    }


    @Override
    public void clickOnLogin() {
// TODO: 23.10.2016  прятать и открывать логин карт панель
        if (getView() != null && getView().getAuthPanel()!=null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
                getView().showMessage("Click on login");
            } else {
                // TODO: 25.10.2016 auth user
                mAuthModel.loginUser(getView().getAuthPanel().getUserEmail(), getView().getAuthPanel().getUserPassword());
                getView().showMessage("request for user auth");
            }
        }
    }

    @Override
    public void clickOnFb() {
        if (getView() != null) {
            getView().showMessage("clickOnFb");
        }
    }

    @Override
    public void clickOnVK() {
        if (getView() != null) {
            getView().showMessage("clickOnVK");
        }
    }

    @Override
    public void clickOnTwitter() {
        if (getView() != null) {
            getView().showMessage("clickOnTwitter");
        }
    }

    @Override
    public void clickOnShowCatalog() {
        if (getView() != null) {
            getView().showMessage("Показать каталог");
            // TODO: 28.10.2016 if update data complete start catalog screen
            getView().showCatalogScreen();
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mAuthModel.isAuthUser();
    }
}
