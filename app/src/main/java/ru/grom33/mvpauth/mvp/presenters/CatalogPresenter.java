package ru.grom33.mvpauth.mvp.presenters;

import java.util.List;

import ru.grom33.mvpauth.data.storage.dto.ProductDto;
import ru.grom33.mvpauth.mvp.models.CatalogModel;
import ru.grom33.mvpauth.mvp.views.ICatalogView;

/**
 * Created by grom on 30.10.2016.
 */
public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter{
    private static CatalogPresenter ourInstance = new CatalogPresenter();
    private CatalogModel mCatalogModel;
    private List<ProductDto> mProductDtoList;

    public static CatalogPresenter getInstance() {
        return ourInstance;
    }

    private CatalogPresenter() {
        mCatalogModel = new CatalogModel();
    }

    @Override
    public void initView() {
        if (mProductDtoList == null){
            mProductDtoList = mCatalogModel.getProductList();
        }
        if (getView() != null){
            getView().showCatalogView(mProductDtoList);
        }
    }

    @Override
    public void clickOnBayButton(int position) {
        if (getView() != null){
           if (checkUserAuth()){
               getView().showAddCartMessage(mProductDtoList.get(position));
           }else{
               getView().showAuthScreen();
           }

        }
    }

    @Override
    public boolean checkUserAuth() {

        return mCatalogModel.isUserAuth();
    }
}
