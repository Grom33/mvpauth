package ru.grom33.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import ru.grom33.mvpauth.mvp.views.IView;

/**
 * Created by grom on 29.10.2016.
 */

public abstract class AbstractPresenter<T extends IView> {
    private T mView;

    public void takeView(T view) {
        mView = view;
    }

    public void dropView() {
        mView = null;
    }

    public abstract void initView();

    @Nullable
    public T getView() {
    return mView;
    }


}
