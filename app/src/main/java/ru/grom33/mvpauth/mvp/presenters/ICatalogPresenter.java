package ru.grom33.mvpauth.mvp.presenters;

/**
 * Created by grom on 30.10.2016.
 */

public interface ICatalogPresenter {
    void clickOnBayButton(int position);
    boolean checkUserAuth();

}
